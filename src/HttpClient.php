<?php

namespace Safeclicks;

use GuzzleHttp\Client;

use Safeclicks\HttpClient\Response;

class HttpClient
{
    
    const UA = 'X-Safeclicks-UA';
    
    /**
     * @var Client The Guzzle client
     */
    private $client;
    
    /**
     * @var string API key
     */
    private $apiKey;
    
    /**
     * @var string Auth token
     */
    private $authToken;
    
    /**
     * @var array Custom parameters
     */
    private $customParams;

    /**
     * @param array Config data
     */
    public function __construct(array $config)
    {
        $this->apiKey = $config['api_key'];
        $this->authToken = $config['auth_token'];
        $this->customParams = !empty($config['custom']) ? $config['custom'] : [];
        
        $this->client = new Client([
                'base_uri'          => $config['api_uri'],
                'timeout'           => $config['api_timeout'],
                'connect_timeout'   => $config['api_connect_timeout'],
                'headers'           => [
                    'User-Agent' => sprintf('%s, %s', static::UA, $config['sdk_version'])
                ]
            ]);
    }

    /**
     * Send data
     * 
     * @param array $postdata
     * @return Response
     */
    public function send(array $postdata)
    {
        $response = new Response;
        
        try {
            $rawResponse = $this->client->request('POST', '/collect', [
                    'form_params' => [
                        'apikey'     => $this->apiKey,
                        'auth_token' => $this->authToken,
                        'd'          => json_encode([
                            'postdata'  => $postdata,
                            'custom'    => $this->customParams
                        ])
                    ]
                ]);
            if ($rawResponse->getStatusCode() == 200) {
                $jsonResponse = json_decode((string) $rawResponse->getBody());
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response->error = sprintf('HTTP %d, %s', $e->getCode(), $e->getMessage());
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            $response->error = $e->getMessage();
        } catch (\Exception $e) {
            $response->error = $e->getMessage();
        }
        
        $response->status = (bool) isset($jsonResponse->status) ? $jsonResponse->status : $response->status;
        $response->error = isset($jsonResponse->error) ? $jsonResponse->error : $response->error;
        $response->message = isset($jsonResponse->message) ? $jsonResponse->message : $response->message;
        $response->tid = isset($jsonResponse->tid) ? $jsonResponse->tid : $response->tid;
        $response->risk = isset($jsonResponse->risk) ? $jsonResponse->risk : $response->risk;
        $response->suggestion = (bool) isset($jsonResponse->suggestion) ? $jsonResponse->suggestion : $response->suggestion;
        
        return $response;
    }

}

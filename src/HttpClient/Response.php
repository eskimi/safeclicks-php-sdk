<?php

namespace Safeclicks\HttpClient;

class Response
{
    
    const MESSAGE_OK = 'OK';
    
    const RISK_NONE         = 'none';
    const RISK_SUSPICIOUS   = 'suspicious';
    const RISK_MALWARE      = 'malware';
    
    /**
     * @var boolean Response status
     */
    public $status = false;
    
    /**
     * @var string|null Error message
     */
    public $error = null;
    
    /**
     * @var string|null Response message
     */
    public $message = null;
    
    /**
     * @var string|null Transaction ID
     */
    public $tid = null;
    
    /**
     * @var boolean Risk type
     */
    public $risk = null;
    
    /**
     * @var boolean Suggestion value. Default TRUE
     */
    public $suggestion = true;
    
    public function __toString()
    {
        $result = [
            'status'    => $this->status,
            'error'     => $this->error,
            'message'   => $this->message,
            'tid'       => $this->tid,
            'risk'      => $this->risk,
            'suggestion'=> $this->suggestion
        ];
        return json_encode($result);
    }
    
}

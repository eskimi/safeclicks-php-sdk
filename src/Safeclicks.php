<?php

namespace Safeclicks;

use Safeclicks\Exception;
use Safeclicks\HttpClient;
use Safeclicks\HttpClient\Response;

final class Safeclicks
{
    
    /**
     * Default custom parameter value
     */
    const DEFAULT_VALUE = 'default';
    
    /**
     * @var string API URI for data submit/collect
     */
    private $apiUriCollect = 'https://api-%s.safeclicks.net/collect';
    
    /**
     * @var string API URI for CSP reports
     */
    private $apiUriCsp = 'https://api-%s.safeclicks.net/cspreport';
    
    /**
     * @var string API URI for getting JS file
     */
    private $apiUriJs = 'https://api-%s.safeclicks.net/js.min.js';
    
    /**
     * @var array Config data
     */
    private $config = [];

    /**
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $config = [])
    {
        if (empty($config['api_region'])) {
            throw new Exception(Exception::API_REGION_MISSING);
        }
        
        $this->apiUriCollect = sprintf($this->apiUriCollect, $config['api_region']);
        $this->apiUriCsp = sprintf($this->apiUriCsp, $config['api_region']);
        $this->apiUriJs = sprintf($this->apiUriJs, $config['api_region']);
        
        $this->config = array_merge($config, [
                'api_uri'               => $this->apiUriCollect,
                'api_timeout'           => 3,
                'api_connect_timeout'   => 2,
                'sdk_version'           => 'PHP SDK v1.0.13'
            ]);
        
        if (empty($this->config['api_key'])) {
            throw new Exception(Exception::API_KEY_MISSING);
        }
        if (empty($this->config['auth_token'])) {
            throw new Exception(Exception::AUTH_TOKEN_MISSING);
        }
        
        foreach (['api_timeout', 'api_connect_timeout'] as $k) {
            if (isset($config[$k]) && $config[$k] >= 0) {
                $this->config[$k] = (int) $config[$k];
            }
        }
    }
    
    /**
     * Get data submit/collect endpoint domain
     * 
     * @return string
     */
    public function getApiDomain()
    {
        return parse_url($this->apiUriCollect, PHP_URL_HOST);
    }
    
    /**
     * Get CSP report endpoint URI
     * 
     * @return string
     */
    public function getCspReportUri()
    {
        $name = '_esc_' . $this->config['api_key'];
        
        $result = sprintf('%s/%s', $this->apiUriCsp, $this->config['api_key']);
        if (!empty($_COOKIE[$name])) {
            $result .= sprintf('/%s', $_COOKIE[$name]);
        }
        return $result;
    }
    
    /**
     * Get JS endpoint URI
     * 
     * @return string
     */
    public function getJsUri()
    {
        return $this->apiUriJs;
    }

    /**
     * Verify form submit data
     * 
     * @param array $postdata
     * @return Response
     */
    public function verify(array $postdata = [])
    {
        $client = new HttpClient($this->config);
        return $client->send($postdata);
    }
    
}

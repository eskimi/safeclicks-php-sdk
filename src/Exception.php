<?php

namespace Safeclicks;

final class Exception extends \Exception
{

    const API_KEY_MISSING = 'API key is required';
    const API_REGION_MISSING = 'API region is required';
    const AUTH_TOKEN_MISSING = 'Auth token is required';

}
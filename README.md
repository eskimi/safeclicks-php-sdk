Safeclicks.net PHP SDK
=============================================================


Dependencies
----------------------------------------

-   [v5.5 <= PHP](http://php.net/downloads.php)



----------------------------------------

Installation can be done using Composer.

```sh
$ composer require eskimi/safeclicks-php-sdk
```


Basic Usage Example
```php
use Safeclicks\Safeclicks;

$config = [
    'api_key'       => 'APPI_KEY',
    'api_region'    => 'APPI_REGION',
    'auth_token'    => 'AUTH_TOKEN'
];

try {
    $safeclicks = new Safeclicks($config);
    $response = $safeclicks->verify($_POST);
    
    if ($response->suggestion) {
        // suggestion to approve form submition
    }
} catch (\Exception $e) {
    error_log('Safeclicks exception: ' . json_encode([$e->getCode(), $e->getMessage()]));
}
```

Set CSP reports
```php
try {
    $safeclicks = new Safeclicks($config);

    header('Content-Security-Policy', sprintf('script-src %s;', implode(' ', [$safeclicks->getApiDomain()]))
            . sprintf('connect-src %s;', implode(' ', [$safeclicks->getApiDomain()]))
            . sprintf('report-uri %s;', implode(' ', [$safeclicks->getCspReportUri()]))
        );
} catch (\Exception $e) {}
```

<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Safeclicks\Safeclicks;

$config = [
    'api_timeout'   => 3,
    'api_key'       => 'APPI_KEY',
    'api_region'    => 'APPI_REGION',
    'auth_token'    => 'AUTH_TOKEN',
    'custom'        => [
        // optional params
        'partner'       => 'Eskimi',
        'service'       => 'Test 123',
        //'product'       => Safeclicks::DEFAULT_VALUE,
        //'advertiser'    => Safeclicks::DEFAULT_VALUE,
        //'msisdn'        => Safeclicks::DEFAULT_VALUE
    ]
];

try {
    $safeclicks = new Safeclicks($config);
    $response = $safeclicks->verify($_POST);
    
    // Set CSP reports (optional)
    header('Content-Security-Policy', sprintf('script-src %s;', implode(' ', [$safeclicks->getApiDomain()]))
            . sprintf('connect-src %s;', implode(' ', [$safeclicks->getApiDomain()]))
            . sprintf('report-uri %s;', implode(' ', [$safeclicks->getCspReportUri()]))
        );
    
    if ($response->status) {
        if ($response->suggestion) {
            // suggestion to approve form submition
        } else {
            // suggestion to block form submition
        }
    } else {
        error_log('Safeclicks invalid response: ' . (string) $response);
    }
} catch (\Exception $e) {
    error_log('Safeclicks exception: ' . json_encode(['code' => $e->getCode(), 'message' => $e->getMessage()]));
}

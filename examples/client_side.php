<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Safeclicks\Safeclicks;

$safeclicksJs = '';
$config = [
    'api_timeout'   => 3,
    'api_key'       => 'APPI_KEY',
    'api_region'    => 'APPI_REGION',
    'auth_token'    => 'AUTH_TOKEN',
    'custom'        => [
        // optional params
        'partner'       => 'Eskimi',
        'service'       => 'Test 123',
        //'product'       => Safeclicks::DEFAULT_VALUE,
        //'advertiser'    => Safeclicks::DEFAULT_VALUE,
        //'msisdn'        => Safeclicks::DEFAULT_VALUE
    ]
];

try {
    $safeclicks = new Safeclicks($config);
    $safeclicksJs = $safeclicks->getJsUri();
} catch (\Exception $e) {
    error_log('Safeclicks exception: ' . json_encode(['code' => $e->getCode(), 'message' => $e->getMessage()]));
}

?><!DOCTYPE html>
<html>
<head>
    <title>Title of the document</title>
    
    <script type="text/javascript"
        data-apikey="<?php echo $config['api_key']?>"
        data-partner="<?php echo $config['custom']['partner']?>"
        data-service="<?php echo $config['custom']['service']?>"
        src="<?php echo $safeclicksJs?>"></script>
</head>
<body>
    The content of the document
</body>
</html>